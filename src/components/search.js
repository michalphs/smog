import * as React from 'react';

import AlgoliaPlaces from 'algolia-places-react';

class Search extends React.Component {
  constructor() {
    super();
    this.state = {
      value:null,
      name:null,
      lat:null,
      lng:null
    }
  }

  //Callback app.js
  getCurrentCoords = (lat,lng) => {
    this.props.setCoords(lat,lng);
    console.log(lat);
  }

  //Geolocalization
  getGeo = () => {
    navigator.geolocation.getCurrentPosition(this.geoSucces, this.geoError);
  }

  geoSucces = (pos) => {
    this.getCurrentCoords(pos.coords.latitude, pos.coords.longitude)
  }

  geoError = (errorObj) => {
    switch (errorObj.code) {
      case errorObj.PERMISSION_DENIED:
        alert('Brak pozwolenia na znalezienie lokalizacji!');
        break;
      case errorObj.POSITION_UNAVAILABLE:
        alert('Brak dostępu do sieci.');
        break;
      case errorObj.TIMEOUT:
        alert('Przekroczono czas oczekiwania.');
        break;
    }
  }
  onClick = () => {
    this.refs.inputRef.autocompleteElem.value = this.state.value
    this.getCurrentCoords(this.state.lat, this.state.lng);
  }
  onChange = ({suggestion}) => {
    this.getCurrentCoords(suggestion.latlng.lat, suggestion.latlng.lng);
  }
  onSuggestion = ({suggestions}) => {
    // console.log(suggestions[0]);
    this.setState({
      name: suggestions[0].name,
      lat:suggestions[0].latlng.lat,
      lng:suggestions[0].latlng.lng,
      value:suggestions[0].value
    });
    
  }
  render() {

    return (
      <nav className='nav'>
        <div className="container nav__container">
          <h1 className="nav__logo">Smog</h1>
          <div className="nav__search">
          <AlgoliaPlaces
            placeholder='Wpisz miejscowść'
            ref='inputRef'
            options={{
              appId: 'pl08S3L7SU3S',
              apiKey: '3cc56ebc042fdd56f1a9d7dfaf065bd2',
              language: 'pl',
              countries: ['pl'],
              type: 'city',
            }}
            onChange={this.onChange}
            onSuggestions={this.onSuggestion}
          />
          <div className="nav__group">
              <button className='nav__btn' disabled={!this.state.lat}  onClick={this.onClick}>Szukaj!</button>
              <button title='Zlokalizuj mnie!' className='nav__btn--geolocation' onClick={this.getGeo}><svg id="nav__svg" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 97.713 97.713"><path id="nav__icon" d="M48.855 0C29.021 0 12.883 16.138 12.883 35.974c0 5.174 1.059 10.114 3.146 14.684 8.994 19.681 26.238 40.46 31.31 46.359a2.003 2.003 0 0 0 3.034 0c5.07-5.898 22.314-26.676 31.311-46.359 2.088-4.57 3.146-9.51 3.146-14.684C84.828 16.138 68.69 0 48.855 0zm0 54.659c-10.303 0-18.686-8.383-18.686-18.686 0-10.304 8.383-18.687 18.686-18.687s18.686 8.383 18.686 18.687c.001 10.303-8.382 18.686-18.686 18.686z" fill="#91DC5A"/></svg></button>
          </div>
          </div>
        </div>
      </nav>
    )
  }
}
export default Search;
