import * as React from 'react';

import Leaflet from 'leaflet';
import { Map, TileLayer, Marker, Popup, Icon } from 'react-leaflet';


class Mapa extends React.Component {
  render() { 
    const position = [this.props.sensorLat, this.props.sensorLng]
    const greenIcon = new Leaflet.Icon({
      iconUrl: require('../img/sensor.png'),
      iconSize:     [32,32], // size of the icon
      popupAnchor:  [0, -15]//
    });
    return (
      <Map center={position} zoom={13} className='air-map'>
        <TileLayer
          attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Marker position={position} icon={greenIcon}>
          <Popup>
           <span>Tutaj znajduje się czujnik powietrza</span>
          </Popup>
        </Marker>
      </Map>
    )
  }
}
export default Mapa;
