import * as React from 'react';

class Results extends React.Component {
  render() { 
    return (
    <div className='air' id='air-wrapper' ref='airRef' >
      <h2 className='air-location'>{this.props.air.locality}</h2>
      <p className='air-street'>Ulica: <span>{this.props.air.street}</span></p>
      <div className='air-quality'>
        <span className='air-quality-box' style={{backgroundColor: this.props.air.airQualityColor }}>{this.props.air.airQuality}</span>
        <p className='air-quality-text'>{this.props.air.airQualityText}</p>
      </div>
      <p className='air-pm25'>PM 25: <span>{this.props.air.pm25}</span></p>
      <p className='air-pm10'>PM 10: <span>{this.props.air.pm10}</span></p>
      <p className='air-temperature'>Temperatura: <span>{this.props.air.temperature}</span></p>
      <p className='air-pressure'>Ciśnienie: <span>{this.props.air.pressure}</span></p>
      <p className='air-humidity'>Wilgotność <span>{this.props.air.humidity}</span></p>
    </div>
    )
  }
}
export default Results;
