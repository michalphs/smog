import axios from 'axios';

export const getMoreInfoAir = function(sensorID) {
  return axios.get(`https://airapi.airly.eu/v1/sensor/measurements?sensorId=${sensorID}`, { 
    headers: {
      'apikey': 'b7b23cc3ea5444399c09dcc4edbb98c6'
    }})
}