import axios from 'axios';

export const getInfoAir = function(lat, lng) {
  return axios.get(`https://airapi.airly.eu/v1/nearestSensor/measurements?latitude=${lat}&longitude=${lng}&maxDistance=100000`, { 
    headers: {
      'apikey': 'b7b23cc3ea5444399c09dcc4edbb98c6'
    }})
}