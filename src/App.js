import * as React from 'react';

import { Component } from 'react';

import Search from './components/search';

import Results from './components/results';

import Map from './components/map';

import { getInfoAir } from './utils/air.js';

import { getMoreInfoAir } from './utils/moreAir.js';

import './style/style.scss';

class App extends Component {
  constructor() {
    super();
    this.state = {
      lat:null,
      lng:null,
      sensorId: null,
      sensorLat:null,
      sensorLng:null,
      locality:null,
      street:null,
      temperature:null,
      airQuality:null,
      airQualityText:null,
      airQualityColor:null,
      pm25:null,
      pm10:null,
      pressure:null,
      humidity:null,
      vendor:null
    }
  }

  setCoords = (lat, lng) => {
    this.setState({ lat, lng });
    this.getAir(lat,lng);
  }

  getAirQualityTextAndColor = (airQuality) => {
    if (airQuality < 20) return {color: '#2ecc71',text: 'Bardzo dobre powietrze'}
    if (airQuality >= 20) return {color: '#27ae60',text: 'Dobre powietrze'}    
    if (airQuality >= 40) return { color: '#e67e22',text: 'Średnie powietrze'}   
    if (airQuality >= 60 ) return { color: '#d35400',text: 'Złe powietrze'}
    if (airQuality >= 80) return { color: '#e74c3c',text: 'Zła jakość powietrza'}
    if (airQuality >= 100) return { color: '#c0392b',text: 'Bardzo zła jakość powietrza'}
  }
  getMoreAir = (sensorId) => {
    getMoreInfoAir(sensorId) 
    .then((response) => {
      const air = response.data.currentMeasurements;
      const noDataString = 'Brak Danych';

      this.setState({
        pressure: air.pressure ? `${Math.round(air.pressure).toString().substr(0,4)} hPa` : noDataString,
        humidity: air.humidity ? `${Math.round(air.humidity)} %` : noDataString,
        temperature: air.temperature ? `${Math.round(air.temperature)} °C` : noDataString
      });
      
      const airQualityTextAndColor = this.getAirQualityTextAndColor(this.state.airQuality);
      this.setState({ airQualityColor: airQualityTextAndColor.color, airQualityText: airQualityTextAndColor.text });
    })
    .catch(function(error) {
      alert('Przepraszamy ale coś poszło nie tak' + error);
    })
  }
  getAir = (lat,lng) => {
    
    getInfoAir(lat, lng)
    .then((response) => {
      const air = response.data;

      const noDataString = 'Brak Danych';

      this.setState({vendor:air.vendor});

      if(this.state.vendor === undefined) {
        alert('Przykro nam, ale nie udało się znaleźć czujnika w Twojej okolicy');
        return;
       
      } else {
        this.getMoreAir(air.id);
        this.setState({
          sensorId:air.id,
          street: air.address.route || noDataString,
          locality: air.address.locality || noDataString,
          airQuality: Math.round(air.airQualityIndex) || noDataString,
          pm10: air.pm10 ? `${Math.round(air.pm10)} ug/m3` : noDataString,
          pm25: air.pm25 ? `${Math.round(air.pm25)} ug/m3` : noDataString,
          sensorLat: parseFloat(air.location.latitude),
          sensorLng: parseFloat(air.location.longitude)
        });
      }
    })
    .catch((error) => {
      alert('Przepraszamy ale coś poszło nie tak' + error);
    })
  }

  CheckResults = () => {
    if(this.state.vendor !== undefined && this.state.lat !== null && this.state.lng !== null) {
      return <Results air={this.state} /> 
    } else {
      return <div></div>;
    }
  }

  CheckSensorCoords = () => {
    if(this.state.vendor !== undefined && this.state.sensorLat !== null && this.state.sensorLng !== null ) {
      return <Map sensorLat={this.state.sensorLat} sensorLng={this.state.sensorLng} />
    } else {
      return <div></div>;
    }
  } 
  render() {
    return (
      <div className='App'>
        <Search setCoords={this.setCoords}/>
        <div className='container container__app'>
          {this.CheckResults()}
          {this.CheckSensorCoords()} 
        </div> 
        <footer className='footer'>
          <div className="container footer__container">
            <span className="footer__made">Made with <span>❤</span> in Poland</span>
            <span className='footer__author'>Created by: <a href="https://michalphs.github.io/" className='footer__link'>Michał Mazur</a></span>      
          </div>      
        </footer>   
      </div>
    );
  }
}

export default App;
